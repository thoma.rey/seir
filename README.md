SEIR

In this notebook, we explore a quite general model with 4 compartments to model the different stages of an infection. We try to apply this model to the coronavirus infection.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/thoma.rey%2Fseir/master?filepath=SEIR.ipynb)
